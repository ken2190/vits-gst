""" from https://github.com/keithito/tacotron """

'''
Defines the set of symbols used in text input to the model.

The default is a set of ASCII characters that works well for English or text that has been run through Unidecode. For other data, you can modify _characters. See TRAINING_DATA.md for details. '''
import os
import sys
base = os.path.dirname(__file__)
sys.path.insert(1,base)
import cmudict

_pad        = '_'
_punctuation = '!\'(),.:;? '
_special = '-'
_end = '~'
# _letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
_letters = 'aáàảạãâấầẩậẫăắằặẵẳbcdđeéèẹẽẻêếềệễểfghiìíịĩỉjklmnoóòọõỏôốồổộỗơớờởợỡpqrstuúùủụũưứừửựữvwxyýỳỵỹỷz'
# Prepend "@" to ARPAbet symbols to ensure uniqueness (some are the same as uppercase letters):
_arpabet = ['@' + s for s in cmudict.valid_symbols]

# Export all symbols:
# symbols = [_pad] + list(_special) + list(_punctuation) + list(_letters) + _arpabet # english ver
symbols = [_pad] + list(_special) + list(_punctuation) + list(_letters)  # 190 ver
#symbols = [_pad] + list(_special) + list(_punctuation) + list(_letters) + list(_end) # 68 ver
# print(symbols[75])
if __name__ =='__main__':
    print(''.join(symbols[x] for x in [81, 13, 31, 48, 11, 82, 60, 13, 59, 11, 81, 13, 31, 48, 11, 82, 93, 11,
                                        59, 48, 49, 40, 59, 11, 95, 14, 11, 97, 17, 11, 48, 70, 49]))